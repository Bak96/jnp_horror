#ifndef CITIZEN_H
#define CITIZEN_H

#include <cassert>
#include <iostream>
#include <type_traits>

template <typename T, int lowerBound, int upperBound, bool isSheriff,
          typename = std::enable_if_t<std::is_arithmetic<T>::value, T>>
class Citizen {
  public:
    template <bool sheriffTemplate = isSheriff,
              typename = std::enable_if_t<!sheriffTemplate>>
    Citizen(T health, T age) : health(health), age(age) {
        assert(age >= lowerBound && age <= upperBound);
        assert(health >= 0);
        assert(age >= 0);
    }

    template <bool sheriffTemplate = isSheriff,
              typename = std::enable_if_t<sheriffTemplate>>
    Citizen(T health, T age, T attackPower)
        : health(health), age(age), attackPower(attackPower) {
        assert(age >= lowerBound && age <= upperBound);
        assert(health >= 0);
        assert(age >= 0);
        assert(attackPower >= 0);
    }

    T getHealth() const { return health; }

    T getAge() const { return age; }

    void takeDamage(T damage) {
        health -= damage;
        if (health < 0) {
            health = 0;
        }
    }

    template <bool sheriffTemplate = isSheriff,
              typename = std::enable_if_t<sheriffTemplate>>
    T getAttackPower() {
        return attackPower;
    }

  private:
    T health;
    T age;
    T attackPower;
};

template <typename T> using Adult = Citizen<T, 18, 100, false>;

template <typename T> using Teenager = Citizen<T, 11, 17, false>;

template <typename T> using Sheriff = Citizen<T, 18, 100, true>;

#endif // CITIZEN_H
