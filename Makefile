CXX=clang
CXXFLAGS=-Wall -std=c++14 -O2 -lstdc++
DEPS= citizen.h monster.h
OBJ= main.o

%.o: %.c $(DEPS)
	$(CXX) $(CXXFLAGS) -c -o $@ $<
	
mainTest: $(OBJ)
	$(CXX) $(CXXFLAGS) -o $@ $^
	
.PHONY: clean

clean:
	rm *.o
