#ifndef SMALLTOWN_H
#define SMALLTOWN_H

#include "citizen.h"
#include "monster.h"

#include <cassert>
#include <cstddef>
#include <cstring>
#include <iostream>
#include <tuple>
#include <typeinfo>

// Auxiliary functions for compile time fibonacci numbers calculation.
namespace {
constexpr long long fibonacci(int n) {
    return n > 1 ? fibonacci(n - 2) + fibonacci(n - 1) : 1;
}

constexpr int fibArraySize(long long n, int i = 0) {
    return fibonacci(i) > n ? i : fibArraySize(n, i + 1);
}

template <class Function, std::size_t... Indices>
constexpr auto make_array_helper(Function f, std::index_sequence<Indices...>)
    -> std::array<typename std::result_of<Function(std::size_t)>::type,
                  sizeof...(Indices)> {
    return {{f(Indices)...}};
}

template <int N, class Function>
constexpr auto make_array(Function f)
    -> std::array<typename std::result_of<Function(std::size_t)>::type, N> {
    return make_array_helper(f, std::make_index_sequence<N>{});
}
}

// Main class
template <typename M, typename U, U t0, U t1, typename... C> class SmallTown {
    static_assert(std::is_integral<U>::value, "U should be integral");
    static_assert(t1 > 0, "t1 should be > 0");
    static_assert(t1 >= t0, "t1 should be >= t0");
    static_assert(t0 >= 0, "t0 should be >= 0");

  public:
    SmallTown(M monster, C... citizens)
        : monster(monster), actualTime(t0), endTime(t1), citizens(citizens...) {
        countAliveCitizens(this->citizens);
    }

    // Firstly checks the current time; if it's time for an attack, it takes an
    // attack on all the citizens. Then adds timestep to the current time.
    void tick(U timeStep) {
        bool monsterAlive = monster.getHealth() > 0;
        bool citizensAlive = numOfAliveCitizens > 0;

        // Checking previous round.
        if (!monsterAlive && citizensAlive) {
            std::cout << "CITIZENS WON" << std::endl;
        } else if (monsterAlive && !citizensAlive) {
            std::cout << "MONSTER WON" << std::endl;
        } else if (!monsterAlive && !citizensAlive) {
            std::cout << "DRAW" << std::endl;
        } else {
            if (isFibonacci(actualTime)) {
                attackAll(citizens);
            }

            actualTime = (actualTime + timeStep) % (endTime + 1);
        }
    }

    // Returns name of a Monster, its health and number of alive citizens.
    std::tuple<std::string, typename M::valueType, size_t> getStatus() {
        return std::make_tuple(monster.getName(), monster.getHealth(),
                               numOfAliveCitizens);
    }

  private:
    int numOfAliveCitizens;
    M monster;
    U actualTime;
    U endTime;

    std::tuple<C...> citizens;

    static constexpr int arraySize =
        fibArraySize(t1); // size of fibonacci numbers array
    static constexpr std::array<long long, arraySize> fibArray =
        make_array<arraySize>(fibonacci); // fibonacci number array

    // Auxiliary functions to count alive citizens and store this count in
    // numOfAliveCitizens variable.
    template <std::size_t I = 0, typename... Tp>
    typename std::enable_if<I == sizeof...(Tp), void>::type
    countAliveCitizens(std::tuple<Tp...> &t) {}

    template <std::size_t I = 0, typename... Tp>
        typename std::enable_if <
        I<sizeof...(Tp), void>::type countAliveCitizens(std::tuple<Tp...> &t) {

        if (I == 0) {
            numOfAliveCitizens = 0;
        }

        if (std::get<I>(t).getHealth() > 0) {
            numOfAliveCitizens++;
        }

        countAliveCitizens<I + 1, Tp...>(t);
    }

    // Simulates monsters attacks on citizens.
    template <std::size_t I = 0, typename... Tp>
    typename std::enable_if<I == sizeof...(Tp), void>::type
    attackAll(std::tuple<Tp...> &t) {}

    template <std::size_t I = 0, typename... Tp>
        typename std::enable_if <
        I<sizeof...(Tp), void>::type attackAll(std::tuple<Tp...> &t) {
        auto &citizen = std::get<I>(t);

        if (citizen.getHealth() > 0) {
            attack(monster, citizen);

            if (citizen.getHealth() == 0) {
                numOfAliveCitizens--;
            }
        }

        attackAll<I + 1, Tp...>(t);
    }

    bool isFibonacci(U n) {
        for (auto it = fibArray.begin(); it != fibArray.end(); ++it) {
            if (*it == n) {
                return true;
            }
        }
        return false;
    }
};

// Declarations
template <typename M, typename U, U t0, U t1, typename... C>
constexpr int SmallTown<M, U, t0, t1, C...>::arraySize;

template <typename M, typename U, U t0, U t1, typename... C>
constexpr std::array<long long, SmallTown<M, U, t0, t1, C...>::arraySize>
    SmallTown<M, U, t0, t1, C...>::fibArray;

#endif // SMALLTOWN_H
