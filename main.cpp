#include "citizen.h"
#include "monster.h"
#include "smalltown.h"

#include <cassert>
#include <tuple>

void adultTest() {
	Adult<int> dorosly(100, 50);

	assert(dorosly.getHealth() == 100);
	assert(dorosly.getAge() == 50);

	dorosly.takeDamage(50);

	assert(dorosly.getHealth() == 50);
	assert(dorosly.getAge() == 50);

	dorosly.takeDamage(50);

	assert(dorosly.getHealth() == 0);
	assert(dorosly.getAge() == 50);

}

void teenagerTest() {
	Teenager<int> nastolatek(50, 14);

	assert(nastolatek.getHealth() == 50);
	assert(nastolatek.getAge() == 14);

	nastolatek.takeDamage(20);

	assert(nastolatek.getHealth() == 30);
	assert(nastolatek.getAge() == 14);

	nastolatek.takeDamage(40);

	assert(nastolatek.getHealth() == 0);
	assert(nastolatek.getAge() == 14);

}

void sheriffTest() {
	Sheriff<int> szeryf(1000, 99, 5);

	assert(szeryf.getHealth() == 1000);
	assert(szeryf.getAge() == 99);
	assert(szeryf.getAttackPower() == 5);

	szeryf.takeDamage(999);

	assert(szeryf.getHealth() == 1);
	assert(szeryf.getAge() == 99);
	assert(szeryf.getAttackPower() == 5);

	szeryf.takeDamage(2);

	assert(szeryf.getHealth() == 0);
	assert(szeryf.getAge() == 99);
	assert(szeryf.getAttackPower() == 5);
}

void zombieTest() {
	Zombie<int> zombiak(100, 10);

	assert(zombiak.getHealth() == 100);
	assert(zombiak.getAttackPower() == 10);

	zombiak.takeDamage(20);

	assert(zombiak.getHealth() == 80);
	assert(zombiak.getAttackPower() == 10);

	zombiak.takeDamage(80);

	assert(zombiak.getHealth() == 0);
	assert(zombiak.getAttackPower() == 10);
}

void vampireTest() {
	Vampire<int> wampir(100, 90);

	assert(wampir.getHealth() == 100);
	assert(wampir.getAttackPower() == 90);

	wampir.takeDamage(50);

	assert(wampir.getHealth() == 50);
	assert(wampir.getAttackPower() == 90);

	wampir.takeDamage(5000);

	assert(wampir.getHealth() == 0);
	assert(wampir.getAttackPower() == 90);
}

void mummyTest() {
	Mummy<int> mumia(100, 5);

	assert(mumia.getHealth() == 100);
	assert(mumia.getAttackPower() == 5);

	mumia.takeDamage(5);

	assert(mumia.getHealth() == 95);
	assert(mumia.getAttackPower() == 5);

	mumia.takeDamage(1000);

	assert(mumia.getHealth() == 0);
	assert(mumia.getAttackPower() == 5);

}

void smallTownTest() {
	Mummy<int> mumia(100, 33);
	Teenager<int> nastolatek(50, 14);
	Adult<int> dorosly(100, 50);
	Sheriff<int> panPolicjant(100, 33, 33);

	SmallTown<Mummy<int>, int, 1, 10000, Teenager<int>, Adult<int>> town(mumia, nastolatek, dorosly);
	for (int i = 0; i < 10; ++i) {
		auto tup = town.getStatus();
		std::cout << std::get<0>(tup) << " " << std::get<1>(tup) << " " << std::get<2>(tup) << std::endl;
		town.tick(1);
	}

	SmallTown<Mummy<int>, int, 1, 100, Sheriff<int>> miasto(mumia, panPolicjant);
	for (int i = 0; i < 10; ++i) {
		auto tup = miasto.getStatus();
		std::cout << std::get<0>(tup) << " " << std::get<1>(tup) << " " << std::get<2>(tup) << std::endl;
		miasto.tick(1);
	}

}

int main() {
	adultTest();
	teenagerTest();
	sheriffTest();

	zombieTest();
	vampireTest();
	mummyTest();

	smallTownTest();
}
