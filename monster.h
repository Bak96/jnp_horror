#ifndef MONSTER_H
#define MONSTER_H

#include <cassert>
#include <iostream>
#include <type_traits>

namespace {
const char VAMPIRE[] = "Vampire";
const char ZOMBIE[] = "Zombie";
const char MUMMY[] = "Mummy";
}

template <typename T, const char *monsterName,
          typename = std::enable_if_t<std::is_arithmetic<T>::value, T>>
class Monster {
  public:
    Monster(T health, T attackPower)
        : health(health), attackPower(attackPower) {
        assert(health >= 0);
        assert(attackPower >= 0);
    }

    T getHealth() const { return health; }

    T getAttackPower() const { return attackPower; }

    const char *getName() { return monsterName; }

    void takeDamage(T damage) {
        health -= damage;
        if (health < 0) {
            health = 0;
        }
    }

    using valueType = T;

  private:
    T health;
    T attackPower;
};

template <typename M, typename U> void attack(M &monster, U &victim) {
    victim.takeDamage(monster.getAttackPower());
}

template <typename M, typename T> void attack(M &monster, Sheriff<T> &victim) {
    victim.takeDamage(monster.getAttackPower());
    monster.takeDamage(victim.getAttackPower());
}

template <typename T> using Zombie = Monster<T, ZOMBIE>;

template <typename T> using Vampire = Monster<T, VAMPIRE>;

template <typename T> using Mummy = Monster<T, MUMMY>;

#endif // MONSTER_H
